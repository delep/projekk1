<?php 
session_start();

include 'koneksi.php';

$username = $_SESSION['username'];

$sql = "SELECT * FROM users";
$result = mysqli_query($koneksi, $sql);

// Fetch all
$hasil = mysqli_fetch_all($result, MYSQLI_ASSOC);
	if ($_SESSION["login"] !== 1 ) {
		header("location: index.php");

	}

if(isset($_GET["pesan"])) {
	if($_GET["pesan"] === "berhasil_hapus") {
		$pesan = "Berhasil menghapus data";
		$warna = "success";
	}
	if($_GET["pesan"] === "berhasil_ubah") {
		$pesan = "Berhasil mengubah data";
		$warna = "success";
	}
	if($_GET["pesan"] === "gagal_hapus") {
		$pesan = "Gagal menghapus data";
		$warna = "danger";
	}
}



 ?>


<?php 

// mengaktifkan session


// untuk mencegah user langsung pergi ke home.php tanpa login
if($_SESSION["login"] !== 1){
    header ("Location:index.php?pesan=Silahkan Login");
}    

?>

<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    
    <!-- Css Ku-->
    <style>
        @font-face {
            font-family: quicksand;
            src: url(Font/Quicksand-Medium.ttf);
        }

        * {
            font-family: quicksand;

        }

        .container {
            font-size: 15px;
        }


        .home-picture img {

            width: 100%;
            background-size: cover;
            background-position: center;
        }
    </style>
    <title>My Blog</title>
</head>

<!--tfft-->
<style type="text/css">
	body {
		 background-image: url(white.jpg);
        background-repeat: no-repeat;
        background-size: cover;
	}
</style>
	<!--tfft-->

<body>

	
    <!-- Navbar -->
    <nav class="navbar navbar-expand-lg navbar navbar-dark bg-dark ">
        <div class="container">
            <a class="navbar-brand " href="home.php" style="font-weight: bold;">
                <i style="font-size: 23px; color: rgb(245, 245, 245);" ></i> <?php echo $_SESSION["username"];?>
               Website</a>

              
                
                <div class="ml-auto navbar-nav">
                    <a type="button"style="width:110px;" class=" btn btn-danger " href="logout.php">Log out</a>
                </div>
        </div>
    </nav>
    <!--Navbar End-->



    <div class="container">
    	<h1>Data User</h1>
    	<a class ="btn btn-primary"href="registrasi.php">Tambah Data</a>

    	<?php
    	if(isset($pesan)){
    		?>
    		<div class="alert alert-<?= $warna ?>" role="alert">
    			<?= $pesan ?>
    		</div>
    		<?php
    	}
    	?>


    <table class="table table-hover">
    	<thead>
    		<tr>
    			<th>No.</th>
    			<th>Nama</th>
    			<th>username</th>
    			<th>Email</th>
    			<th>Avatar</th>
    			<th>Action</th>
    		</tr>
    	</thead>

    	<tbody>
    		<?php
    		foreach ($hasil as $key => $user_data) {
    			?>
    			<tr>
    				<td><?= $key + 1 ?></td>
    				<td><?= $user_data["nama"] ?></td>
    				<td><?= $user_data["username"] ?></td>
    				<td><?= $user_data["email"] ?></td>
    				<td><?= $user_data["avatar"] ?></td>
    				<td>
    					<a class="btn btn-success" href="form_ubah.php?id=<?=$user_data['id']?>">Ubah</a>
    					<a class="btn btn-danger" href="proses_hapus.php?id=<?=$user_data['id']?>">Hapus</a>
    				</td>
    			</tr>
    			<?php
    		}
    		?>
    	</tbody>
    	<tfoot>
    		
    	</tfoot>
    </table>



    </div>


    <center>
                
            </center>


		<div class="footer">
  
  <p>&copy; Copyright By Ahmad Dzlfikar As Shavy</p>

</div>
<style type="text/css">
	.footer {
   position: fixed;
   left: 0;
   bottom: 0;
   width: 100%;
   background-color: transparent;
   color: #adadad;
   text-align: left;
   padding: 15px 0px 0px 10px; }
</style>


   

        <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>
</body>

</html>